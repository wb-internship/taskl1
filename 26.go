package main

import (
	"fmt"
)

/*Разработать программу, которая проверяет, что все символы
в строке уникальные (true — если уникальные, false etc).
Функция проверки должна быть регистронезависимой.
Например:
abcd — true
abCdefAaf — false
aabcd — false*/

// IsUniqueString возвращает true, если строка состоит
// из уникальных символов, иначе false
func IsUniqueString(str string) bool {
	// Множество уникальных символов
	stringSet := make(map[rune]struct{})
	for _, r := range str {
		// Если такой символ уже есть во сете, возвращаем false
		if _, ok := stringSet[r]; ok {
			return false
		}
		// Если символ ранее не встречался, добавляем его в сет
		stringSet[r] = struct{}{}
	}
	return true
}

func main() {
	listOfString := []string{"abcd", "abCdefAaf", "aabcd", "aA", "  ", ""}
	for _, str := range listOfString {
		fmt.Printf("\"%s\" is %v\n", str, IsUniqueString(str))
	}
}
