package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

/*Реализовать структуру-счетчик, которая будет инкрементироваться
в конкурентной среде. По завершению программа должна выводить
итоговое значение счетчика.*/

type ConCount struct {
	mu      sync.Mutex
	counter int
}

func Count(channel chan struct{}, counter *int64, wg *sync.WaitGroup) {
	for range channel {
		*counter++
	}
	wg.Done()

}

func main() {

	// Счетчик с Mutex
	counter1 := ConCount{counter: 0}

	// Атомарный счетчик
	var counter2 int64
	var counter2_1 int64

	// Канал для передачи сигнала
	var counter3 int64
	channel := make(chan struct{})
	var wgChan sync.WaitGroup
	wgChan.Add(1)
	go Count(channel, &counter3, &wgChan)

	var wg sync.WaitGroup
	n := 1000
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			for i := 0; i < 10; i++ {
				counter1.mu.Lock()
				counter1.counter++
				counter1.mu.Unlock()

				atomic.AddInt64(&counter2, 1)
				counter2_1++

				channel <- struct{}{}
			}
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("mutex ", counter1.counter)
	fmt.Println("atomic", counter2)
	fmt.Println("not atomic", counter2_1)
	fmt.Println("chanel ", counter3)
}
