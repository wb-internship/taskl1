package main

import (
	"fmt"
	"time"
)

/*Разработать программу, которая будет последовательно отправлять значения
в канал, а с другой стороны канала — читать. По истечению N секунд
программа должна завершаться.*/

func main() {
	n := 1

	// В канал stop будет записано текущее время по истечению n секунд
	stop := time.After(time.Duration(n) * time.Second)

	c := make(chan int, 1)

	// Запись в канал
	go func() {
		for i := 0; ; i++ {
			fmt.Printf("write %d\n", i)
			c <- i
			time.Sleep(100 * time.Millisecond)
		}
	}()

	// Чтение из канала
	go func() {
		for value := range c {
			fmt.Printf("read %d\n", value)
		}
	}()

	// main заблокирован до истечения времени, переданного в time.After
	<-stop

	// Второй варинат просто main увести в слип на n секунд
	//time.Sleep(time.Duration(n) * time.Second)
}
