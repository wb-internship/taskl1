package main

import (
	"fmt"
	"math/rand"
)

/*Реализовать пересечение двух неупорядоченных множеств.*/

// GenSet сгенерировать множество размера n из случайых чисел из диапозона [0,k)
func GenSet(k int, n int) map[int]struct{} {
	set := make(map[int]struct{})
	for i := 0; i < n; i++ {
		set[rand.Intn(k)] = struct{}{}
	}
	return set
}

// Intersect возвращает пересечение двух множеств
func Intersect(set1 map[int]struct{}, set2 map[int]struct{}) map[int]struct{} {
	set := make(map[int]struct{})

	// Проходимся по элементам меньшего по размеру сета
	if len(set1) > len(set2) {
		set1, set2 = set2, set1
	}

	for item := range set1 {
		if _, ok := set2[item]; ok {
			set[item] = struct{}{}
		}
	}

	return set
}

func main() {
	set1 := GenSet(10, 10)
	set2 := GenSet(10, 10)

	fmt.Println("set1:", set1)
	fmt.Println("set2:", set2)

	set3 := Intersect(set1, set2)
	fmt.Println("intersect:", set3)
}
