package main

import (
	"fmt"
	"math/rand"
)

/*Реализовать быструю сортировку массива (quicksort) встроенными методами языка.*/

func newRandIntSlice(k int, n int) []int {
	s := make([]int, n)
	for i := 0; i < n; i++ {
		s[i] = (rand.Intn(2)*2 - 1) * rand.Intn(k)
	}
	return s
}

func partition(arr []int, l, r int) int {
	pivot := arr[r]
	i := l
	for j := l; j < r; j++ {
		if arr[j] < pivot {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	arr[i], arr[r] = arr[r], arr[i]
	return i
}

func quickSort(arr []int, l, r int) {
	if l < r {
		var p int
		p = partition(arr, l, r)
		quickSort(arr, l, p-1)
		quickSort(arr, p+1, r)
	}
}

func quickSortStart(arr []int) {
	quickSort(arr, 0, len(arr)-1)
}

func main() {
	s := newRandIntSlice(100, 10)
	fmt.Println(s)
	quickSortStart(s)
	fmt.Println(s)
}
