package main

/*Написать программу, которая конкурентно рассчитает значение квадратов чисел
взятых из массива (2,4,6,8,10) и выведет их квадраты в stdout.*/

import (
	"fmt"
	"sync"
)

func main() {
	var nums = []int{2, 4, 6, 8, 10}

	// WaitGroup для рутин
	var wg sync.WaitGroup
	for _, num := range nums {
		// Добавляем 1 в счетчик wg перед запуском очередной рутины
		wg.Add(1)
		// Вызывается рутина
		go func(num int) {
			square(num)
			// По завершению работы рутины декрементируем каунтер в wg
			wg.Done()
		}(num)
	}
	// Ждем завершения работы всех рутин.
	wg.Wait()
}

func square(num int) {
	pow := num * num
	fmt.Println(pow)
}
