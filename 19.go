package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*Разработать программу, которая переворачивает подаваемую на ход строку
(например: «главрыба — абырвалг»). Символы могут быть unicode.*/

func Reverse(str string) string {
	rev := make([]rune, len(str))
	fmt.Println(len(rev))
	i := len(str) - 1
	for _, char := range str {
		rev[i] = char
		i--
	}
	return string(rev)
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("String: ")
	in, _ := reader.ReadString('\n')
	s := strings.TrimSuffix(in, "\n")
	fmt.Println("Reversed:", Reverse(s))
}
