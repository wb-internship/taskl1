package main

import "fmt"

/*К каким негативным последствиям может привести данный фрагмент кода, и как
это исправить? Приведите корректный пример реализации.

var justString string
func someFunc() {
	v := createHugeString(1 << 10)
	justString = v[:100]
}
func main() {
	someFunc()
}*/

/*
В этом фрагменете кода 2 проблемы:
 1. При таком способе взять подстроку v[:100], мы получим первые 100 байтов, вместа первых 100 симвлов, если у нас есть
    не только символы ASCII
 2. Глобальная переменная ссылается на большую строку, которая определена внутри функции, что не позваляет очистить эту большую строку из памяти
*/
var justString string

func createHugeString(n int) string {
	return "like a huge string типа большая строка"
}
func someFunc() {
	v := createHugeString(1 << 10)

	fmt.Println(v[:30]) //показываем проблему Юникод символов

	rStr := make([]rune, 30)   //создадим слайс рунов на необходимое количество символов
	copy(rStr, []rune(v)[:30]) //скопируем подстроку в слайс рунов, чтобы глобальная переменная justString не ссылалась на большую строку
	justString = string(rStr)
	fmt.Println(justString)
}

func main() {
	someFunc()
}
