package main

import (
	"fmt"
	"math"
	"strconv"
)

/*Дана последовательность температурных колебаний: -25.4, -27.0 13.0, 19.0,
15.5, 24.5, -21.0, 32.5. Объединить данные значения в группы с шагом в 10
градусов. Последовательность в подмножноствах не важна.
Пример: -20:{-25.0, -27.0, -21.0}, 10:{13.0, 19.0, 15.5}, 20: {24.5}, etc.*/

func main() {
	temps := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5, -7.8, 3.5, 0.0}

	//Первый вариант, как в примере(-10 и +10 находятся в одном диапозоне)
	subset1 := make(map[int][]float64)
	for _, temp := range temps {
		key := int(temp/10) * 10
		subset1[key] = append(subset1[key], temp)
	}
	fmt.Println(subset1)

	//Второй вариант
	subset2 := make(map[string][]float64)
	for _, temp := range temps {
		key := int(math.Floor(temp/10)) * 10
		keyStr := strconv.Itoa(key)
		if key < 0 {
			key += 10
			keyStr = strconv.Itoa(key)
			if key == 0 {
				keyStr = "-0"
			}
		}
		subset2[keyStr] = append(subset2[keyStr], temp)
	}
	fmt.Println(subset2)

}
