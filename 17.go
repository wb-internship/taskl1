package main

import (
	"fmt"
	"math/rand"
	"sort"
)

/*Реализовать бинарный поиск встроенными методами языка.*/

func newRandIntSortedSlice(k int, n int) []int {
	s := make([]int, n)
	for i := 0; i < n; i++ {
		s[i] = (rand.Intn(2)*2 - 1) * rand.Intn(k)
	}
	sort.Ints(s)
	return s
}

func BinarySearch(s []int, x int) (c int) {
	l := 0
	r := len(s) - 1
loop:
	for l <= r {
		c = (l + r) / 2
		switch {
		case s[c] == x:
			break loop
		case s[c] < x:
			l = c + 1
		case s[c] > x:
			r = c - 1
		}
	}
	return c
}

func main() {
	arr := newRandIntSortedSlice(100, 2)
	fmt.Println(arr)

	x := arr[rand.Intn(len(arr))]
	fmt.Printf("search value = %d\n", x)
	i := BinarySearch(arr, x)
	fmt.Printf("BinarySearch: found s[%d] = %d\n", i, arr[i])
}
