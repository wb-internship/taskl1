package main

import (
	"fmt"
	"math/big"
	"math/rand"
)

/*Разработать программу, которая перемножает, делит, складывает,
вычитает две числовых переменных a, b, значение которых > 2^20.*/

func sum(a, b *big.Int) *big.Int {
	var c big.Int
	c.Add(a, b)
	return &c
}

func dif(a, b *big.Int) *big.Int {
	var c big.Int
	c.Neg(b)
	return sum(a, &c)
}

func mul(a, b *big.Int) *big.Int {
	var c big.Int
	c.Mul(a, b)
	return &c
}

func div(a, b *big.Int) *big.Int {
	var c big.Int
	c.Quo(a, b)
	return &c
}

func main() {
	//сгенерируем числа >=10^20, что во много раз больше, чем 2^20
	var letterRunes = []rune("1234567890")
	aStr := make([]rune, rand.Intn(20)+20)
	bStr := make([]rune, rand.Intn(20)+20)
	for i := range aStr {
		aStr[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	for i := range bStr {
		bStr[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	a := new(big.Int)
	b := new(big.Int)
	a.SetString(string(aStr), 10)
	b.SetString(string(bStr), 10)

	fmt.Printf("a = %v, b = %v\n", a, b)
	fmt.Printf("a + b = %v\n", sum(a, b))
	fmt.Printf("a - b = %v\n", dif(a, b))
	fmt.Printf("a * b = %v\n", mul(a, b))
	fmt.Printf("a / b = %v\n", div(a, b))
}
