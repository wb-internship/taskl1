package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

/*Дана последовательность чисел: 2,4,6,8,10. Найти сумму их
квадратов(2^2+3^2+4^2….) с использованием конкурентных вычислений.*/

func main() {
	var nums = []int{2, 4, 6, 8, 10}
	// Вариант 1
	// Канал для записи результатов вычислений рутин
	SquaresChanel := make(chan int)
	for _, num := range nums {
		go func(num int) {
			// Записываем квдрат числа в канал
			SquaresChanel <- num * num
		}(num)
	}

	// Вычисляем сумму, считывая квадраты из канала столько раз, сколько чисел мы отправили на вычисление их квадратов
	sum := 0
	for range nums {
		sum += <-SquaresChanel
	}

	fmt.Println(sum)

	//Вариант 2
	// Вычисление суммы внутри рутины
	var wg sync.WaitGroup
	var sum1 int64
	for _, num := range nums {
		wg.Add(1)
		go func(num int) {
			atomic.AddInt64(&sum1, int64(num*num))
			wg.Done()
		}(num)
	}
	wg.Wait()
	fmt.Println(sum1)

}
