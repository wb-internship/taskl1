package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
)

/*Реализовать паттерн «адаптер» на любом примере.*/

// XmlInterface есть вот такой интерфейс
type XmlInterface interface {
	GetXml() ([]byte, error)
}

// XmlData и вот такая структура
type XmlData struct {
	xmlBlob []byte
}

func (x *XmlData) GetXml() (string, error) {
	return string(x.xmlBlob), nil
} //эта структура удовлетворяет интерфейсу

// JsonData другая структура
type JsonData struct {
	jsonBlob []byte
}

func (j *JsonData) GetJson() ([]byte, error) {
	return j.jsonBlob, nil
} // и эта структура не удовлетворяет интерфейсу

// Создадаим адаптер который будет работать с неудовлетворяющей интерфейсу структурой
type jsonAdapter struct {
	*JsonData
}

type DataDto struct {
	FieldString string
	FieldInt    int
}

// GetXml Адаптер уже будет удовляетворять интерфейсу
func (ja *jsonAdapter) GetXml() (string, error) {
	var data DataDto
	jsonBlob, _ := ja.GetJson()
	if err := json.Unmarshal(jsonBlob, &data); err != nil {
		return "", err
	}
	res, err := xml.Marshal(&data)
	if err != nil {
		return "", err
	}
	return string(res), nil
}

func main() {

	xmlData := XmlData{
		[]byte(`<Data><FieldString>qwer</FieldString><FieldInt>123</FieldInt></Data>`),
	}

	xmlStr, _ := xmlData.GetXml()
	fmt.Println(xmlStr)

	jsonData := &JsonData{
		[]byte(`{"FieldString":"asdf", "FieldInt":456}`),
	}
	jsonAdapter := jsonAdapter{jsonData}

	jsonStr, err := jsonAdapter.GetXml()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(jsonStr)
}
