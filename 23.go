package main

import (
	"fmt"
	"math/rand"
	"reflect"
	"unsafe"
)

/*Удалить i-ый элемент из слайса.*/
type task23 struct {
}

func (t *task23) newRandIntSlice(k int, n int) []int {
	s := make([]int, n)
	for i := 0; i < n; i++ {
		s[i] = (rand.Intn(2)*2 - 1) * rand.Intn(k)
	}
	return s
}

func main() {
	this := task23{}
	slice := this.newRandIntSlice(10, 10)
	i := 5
	fmt.Println(slice)
	fmt.Println(*(*reflect.SliceHeader)(unsafe.Pointer(&slice)))

	//удаление с копирование второй части
	copy(slice[i:], slice[i+1:])
	slice = slice[:len(slice)-1]
	fmt.Println(slice)
	fmt.Println(*(*reflect.SliceHeader)(unsafe.Pointer(&slice)))

	//удаление с копирование одного элемента(не сохранятеся последовательность)
	slice[i], slice[len(slice)-1] = slice[len(slice)-1], 0
	slice = slice[:len(slice)-1]
	fmt.Println(slice)
	fmt.Println(*(*reflect.SliceHeader)(unsafe.Pointer(&slice)))

	//составлене слайса из части до и после i элемента
	slice = append(slice[:i], slice[i+1:]...)
	fmt.Println(slice)
	fmt.Println(*(*reflect.SliceHeader)(unsafe.Pointer(&slice)))
}
