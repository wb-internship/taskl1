package main

import "fmt"

/*Поменять местами два числа без создания временной переменной.*/

func main() {
	a := 1
	b := 2
	fmt.Println(a, b)

	//Первый способо
	a, b = b, a
	fmt.Println(a, b)

	//Второй способ
	a = a + b
	b = a - b
	a = a - b
	fmt.Println(a, b)
}
