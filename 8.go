package main

import (
	"fmt"
	"math/rand"
)

/*Дана переменная int64.
Разработать программу которая устанавливает i-й бит в 1 или 0.*/

// On устанавливает i-й бит в 1, используя побитовое ИЛИ
// с маской типа 00100 с единицей в позиции i
func On(n int64, i int) int64 {
	return n | 1<<(i-1)
}

// Off устанавливает i-й бит в 0, используя побитовое И
// с маской типа 11011 с нулем в позиции i
func Off(n int64, i int) int64 {
	return n & ^(1 << (i - 1))
}

func main() {
	// Случайное число типа int64
	// Первый rand генерирует -1 или 1
	num := (rand.Int63n(2)*2 - 1) * rand.Int63()

	i := rand.Intn(64)
	on := On(num, i)
	off := Off(num, i)

	fmt.Printf("i=%d\n", i)
	fmt.Printf("%d: %b\n", num, uint64(num))
	fmt.Printf("%d: %b\n", on, uint64(on))
	fmt.Printf("%d: %b\n", off, uint64(off))
}
