package main

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

/*Реализовать постоянную запись данных в канал (главный поток). Реализовать
набор из N воркеров, которые читают произвольные данные из канала и выводят
в stdout. Необходима возможность выбора количества воркеров при старте.

Программа должна завершаться по нажатию Ctrl+C. Выбрать и обосновать
способ завершения работы всех воркеров.*/

import (
	"math/rand"
)

// enableWorkers запускает n рутин, читающих из канала c
func enableWorkers(n int, c chan int) {
	for i := 0; i < n; i++ {
		go func(num int) {
			// Читаем из канала, пока он не закрыт
			for {
				if value, ok := <-c; ok != false {
					fmt.Println(strconv.Itoa(num) + ": " + strconv.Itoa(value))
				} else {
					break
				}
			}
		}(i)
	}
}

func main() {
	chanel := make(chan int)

	// Канал для записи сигнала ОС
	osSigint := make(chan os.Signal, 1)
	// Если поступил SIGINT, генерируемый Ctrl+C, в канал будет записано значение
	signal.Notify(osSigint, syscall.SIGINT)

	// Задаем количество воркеров
	reader := bufio.NewReader(os.Stdin)
	var n int
	var err error
	for {
		fmt.Print("Enter numbers of workers: ")
		nStr, _, _ := reader.ReadLine()
		n, err = strconv.Atoi(string(nStr))
		if err != nil {
			fmt.Println("It's not a numbers. Try again please." + err.Error())
			continue
		}
		break
	}

	//Запускаем воркеры, которые читают из канала
	enableWorkers(n, chanel)

	// Записываем в канал случайные числа, пока не поступит сигнал SIGINT
loop:
	for {
		select {
		case <-osSigint:
			close(chanel)
			fmt.Println("end")
			break loop
		default:
			chanel <- rand.Int()
		}
	}
}
