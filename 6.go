package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

/*Реализовать все возможные способы остановки выполнения горутины.*/

func main() {
	var wg sync.WaitGroup

	// Читающая из канала рутина завершит работу при закрытии канала
	c := make(chan int)
	wg.Add(1)
	go func() {
		fmt.Println("routine started")
		for i := range c {
			fmt.Printf("i've read %d\n", i)
		}
		fmt.Println("routine stopped")
		wg.Done()
	}()
	for i := 0; i < 5; i++ {
		c <- i
	}
	fmt.Println("closing chan")
	close(c)
	wg.Wait()

	// Специальный канал, при отправке в который рутина читает из него и завершает работу
	stop := make(chan bool)
	wg.Add(1)
	go func() {
		fmt.Println("routine started")
		i := 0
		for {
			select {
			case <-stop:
				fmt.Println("routine stopped")
				wg.Done()
				return
			default:
				i++
				fmt.Printf("tick: %d\n", i)
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()

	time.Sleep(500 * time.Millisecond)
	fmt.Println("sending to chan")
	stop <- true
	wg.Wait()

	// Специальный канал, при закрытии которого рутина завершает работу, так можно завершить нескольких рутин
	done := make(chan bool)
	f := func(num int) {
		fmt.Printf("routine %d started\n", num)
		for i := 0; ; i++ {
			select {
			case _, ok := <-done:
				if ok == false {
					fmt.Printf("routine %d stopped\n", num)
					wg.Done()
					return
				}
			default:
				fmt.Printf("routine %d: tick: %d\n", num, i)
			}
			time.Sleep(100 * time.Millisecond)
		}
	}
	wg.Add(2)
	go f(1)
	go f(2)

	time.Sleep(500 * time.Millisecond)
	fmt.Println("closing chan")
	close(done)
	wg.Wait()

	// Передать в рутину контектс со специальным каналом  Done
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(1)
	go func(ctx context.Context) {
		fmt.Println("routine started")
		i := 0
		for {
			select {
			case <-ctx.Done():
				fmt.Println("routine stopped")
				wg.Done()
				return
			default:
				i++
				fmt.Printf("tick: %d\n", i)
			}
			time.Sleep(100 * time.Millisecond)
		}
	}(ctx)
	time.Sleep(500 * time.Millisecond)
	fmt.Println("canceling")
	// Канал контектса закрывается, при вызове возврщенной функции из context.WithCancel
	cancel()
	wg.Wait()

	// Завершение работы при условии (значение переменной)
	var a int64
	wg.Add(1)
	go func() {
		fmt.Println("routine started")
		for a < 5 {
			fmt.Printf("a value: %d\n", a)
			time.Sleep(100 * time.Millisecond)
		}
		fmt.Println("routine stopped")
		wg.Done()
	}()

	fmt.Println("incrementing a")
	for i := 0; i < 5; i++ {
		time.Sleep(90 * time.Millisecond)
		a++
	}
	wg.Wait()

}
