package main

import (
	"fmt"
	"strings"
)

/*Разработать программу, которая переворачивает слова в строке.
Пример: «snow dog sun — sun dog snow».*/

func RevWords(str string) string {
	arr := strings.Fields(str)
	length := len(arr)
	for i := 0; i < length/2; i++ {
		arr[i], arr[length-i-1] = arr[length-i-1], arr[i]
	}
	return strings.Join(arr, " ")
}

func main() {
	s := "snow dog sun песок кот луна"
	fmt.Println("String:", s)
	fmt.Println("RevWords:", RevWords(s))
}
