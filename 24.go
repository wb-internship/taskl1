package main

import (
	"fmt"
	"math"
)

/*Разработать программу нахождения расстояния между двумя точками, которые
представлены в виде структуры Point с инкапсулированными параметрами x,y и
конструктором.*/

// Point содержит приватные поля x, y, сетятся в конструкторе, значения доступны через геттеры
type Point struct {
	x float64
	y float64
}

// NewPoint Конструктор
func NewPoint(x, y float64) *Point {
	return &Point{x, y}
}

// GetX Геттер
func (p *Point) GetX() float64 {
	return p.x
}

// GetY Геттер
func (p *Point) GetY() float64 {
	return p.y
}

// Distance возвращает расстояние между точками a, b *Point
func Distance(a, b *Point) float64 {
	return math.Sqrt(math.Pow(a.GetX()-b.GetX(), 2) + math.Pow(a.GetY()-b.GetY(), 2))
}

func main() {
	point1 := NewPoint(-1.57, 5.64)
	point2 := NewPoint(12.2, 2.01)

	fmt.Println(Distance(point1, point2))

}
