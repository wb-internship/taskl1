package main

import (
	"fmt"
	"reflect"
)

/*Разработать программу, которая в рантайме способна определить тип
переменной: int, string, bool, channel из переменной типа interface{}.*/

func main() {

	vars := []interface{}{1, "hello", true, make(chan int), make(map[string]struct{})}

	var types []string
	//1
	for _, v := range vars {
		switch v.(type) {
		case int:
			types = append(types, "\"int\"")
		case string:
			types = append(types, "\"string\"")
		case bool:
			types = append(types, "\"bool\"")
		case chan int:
			types = append(types, "\"chan int\"")
		default:
			types = append(types, "\"other type\"")
		}
	}
	fmt.Println("Switch:", types)
	types = types[:0]

	//2
	for _, v := range vars {
		types = append(types, "\""+reflect.TypeOf(v).String()+"\"")
	}
	fmt.Println("reflect.TypeOf:", types)
	types = types[:0]

	//3
	for _, v := range vars {
		types = append(types, "\""+reflect.ValueOf(v).Kind().String()+"\"")
	}
	fmt.Println("reflect.ValueOf.Kind:", types)
	types = types[:0]

	//4
	for _, v := range vars {
		types = append(types, fmt.Sprintf("\"%T\"", v))
	}
	fmt.Println("printf:", types)
}
