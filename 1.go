package main

import "fmt"

/*Дана структура Human (с произвольным набором полей и методов).
Реализовать встраивание методов в структуре Action от родительской
структуры Human (аналог наследования).*/

// Human структура с полем Name и методом Say()
type Human struct {
	Name string
}

func (h Human) Say() {
	fmt.Println("I'm " + h.Name)
}

// Dog структура с полем Name и методом Voice()
type Dog struct {
	Sound string
}

func (d Dog) Voice() {
	fmt.Println(d.Sound)
}

// Action структура, в которую встроены Human и Dog
type Action struct {
	Human
	Dog
}

func main() {
	h := Human{
		Name: "Grisha",
	}
	d := Dog{
		Sound: "Bark!",
	}

	a := Action{
		Human: Human{
			Name: "Sasha",
		},
		Dog: Dog{
			Sound: "Bark! Bark!",
		},
	}

	// Вызов методов Say() и Voice() у структур Human и Dog
	h.Say()
	d.Voice()
	// Output:
	//		I'm Grisha
	//		Bark!

	// У Action можно вызывать методы встроенных структур
	a.Say()
	a.Voice()
	// Output:
	//		I'm Sasha
	//		Bark! Bark!
}
